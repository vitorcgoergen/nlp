## Utilities file with functions to be used for tweet sentiment analysis

import nltk
from nltk.corpus import twitter_samples
import matplotlib.pyplot as plt
import random
import re                                  # library for regular expression operations
import string                              # for string operations
from nltk.corpus import stopwords          # module for stop words that come with NLTK
from nltk.stem import PorterStemmer        # module for stemming
from nltk.tokenize import TweetTokenizer   # module for tokenizing strings


# Function to preprocess the tweets (A memory party, as implemented by Coursera's profs)
def preprocess_tweet(tweet):
    # Remove hyperlinks and tweeter marks (RT and hashtags)
    newtweet = re.sub(r'^RT[\s]+', '', tweet)
    newtweet = re.sub(r'https?:\/\/.*[\r\n]*', '', newtweet)
    newtweet = re.sub(r'#','',newtweet)

    # Tokenizing 
    tokenizer = TweetTokenizer(preserve_case=False, strip_handles=True,
                               reduce_len=True)
    tweet_tokens = tokenizer.tokenize(newtweet)
    
    # Remove stopwords and punctuation
    stopwords_english = stopwords.words('english')
    tweets_clean = []
    for word in tweet_tokens:
        if (word not in stopwords_english and
            word not in string.punctuation):
            tweets_clean.append(word)

    # Stemming words using the Porter stemmer algorithm
    stemmer = PorterStemmer()
    tweets_stem = [] 
    for word in tweets_clean:
        stem_word = stemmer.stem(word)
        tweets_stem.append(stem_word)
    return tweets_stem    

# Function to create the frequencies table
def build_freqs(tweets,ys):
    dictio = {}
    # Creating a list of ys from the np array
    yslist = np.squeeze(ys).tolist()

    # Iterating over the tweets
    for i in range(0,len(yslist)):
        for word in preprocess_tweet(tweets[i]):
            pair = (word,yslist[i])
            dictio[pair] = dictio.get(pair,0) + 1
    
    return dictio
    
